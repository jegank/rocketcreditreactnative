import React, { Component } from 'react';
import { View, Text, StyleSheet, label, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import Form from '../../../applicationComponent/component/Form';
import FieldsContainer from '../../../applicationComponent/component/FieldsContainer';
import FieldSet from '../../../applicationComponent/component/FieldSet';
import FileUpload from '../../../applicationComponent/component/FileUpload';
import Label from '../../../applicationComponent/component/Label';
import { Actions } from 'react-native-router-flux';
import Header from '../../../applicationComponent/component/Header';
import SubmitButton from '../../../applicationComponent/component/SubmitButton';
import { saveVerifyDetails } from '../../../redux/actions/verify.details.action';



class VerifyDetails extends Component {

    constructor(props) {
        super(props)
        this.backPage = this.backPage.bind(this);
        this.onSubmit = this.onSubmit.bind(this);


    }

    backPage() {
        Actions.takeLoan();
    }

    onSubmit() {
        let { submitVerifyDetails } = this.props;
        submitVerifyDetails();
        Actions.verifyDetailsGovProof();
    };

    render() {

        let { handleSubmit } = this.props;

        return (
            <View style={{ flex: 1 }}>
                <Header backPage={this.backPage} label={"Verify Details"} helpText={"We need a few more details to get started"} />
                <Form>
                    <FieldSet label="Identification" >
                        <FieldsContainer>
                            <FieldSet label="Payslips" >
                                <FieldSet label="please upload last 3 months payslips" >

                                </FieldSet>
                                <View style={{ padding: 1, margin: 15, flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Field name="payslipOne" component={FileUpload} />
                                    <Field name="payslipTwo" component={FileUpload} />
                                    <Field name="payslipThree" component={FileUpload} />
                                </View>
                            </FieldSet>
                        </FieldsContainer>
                        <FieldsContainer>
                            <FieldSet label="Bank Statements" >
                                <FieldSet label="please upload last 3 months bankStatements" >

                                </FieldSet>
                                <View style={{ padding: 1, margin: 15, flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Field name="bankStatementOne" component={FileUpload} />
                                    <Field name="bankStatementTwo" component={FileUpload} />
                                    <Field name="bankStatementThree" component={FileUpload} />
                                </View>
                            </FieldSet>
                        </FieldsContainer>
                        <FieldsContainer>
                            <SubmitButton label={"NEXT"} handleSubmit={handleSubmit} onSubmit={this.onSubmit} />
                        </FieldsContainer>
                    </FieldSet>
                </Form ></View>);
    }
}

const mapStateToProps = (state) => {
    // console.log("------>", state.form.verifyDetails);
    return {
        initialValues: {
            payslipOne: "",
            payslipTwo: "Feb.png",
            payslipThree: "mar.png",
            bankStatementOne: "jan.png",
            bankStatementTwo: "Feb.png",
            bankStatementThree: "Mar.png"
        }
    }
}
const form = reduxForm({
    form: 'verifyDetails'
})(VerifyDetails)


const mapDispatchToProps = (dispatch) => {
    return {
        submitVerifyDetails: () => dispatch(saveVerifyDetails())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(form)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
    },
    button: {
        width: 300,
        backgroundColor: 'red',
        borderRadius: 25,
        marginVertical: 25,
        paddingVertical: 13,
        alignItems: 'center',
        justifyContent: 'center'

    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center',
    },
    signupText: {
        color: 'rgba(255,255,255,0.6)',
        fontSize: 16
    }

});