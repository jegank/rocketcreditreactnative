import React, { Component } from 'react';
import { View, Text, StyleSheet, label, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import Form from '../../../applicationComponent/component/Form';
import { Actions } from 'react-native-router-flux';
import Header from '../../../applicationComponent/component/Header';
import SubmitButton from '../../../applicationComponent/component/SubmitButton';
import { saveVerifyDetailsGovProof } from '../../../redux/actions/verify.details.action';
import Select from '../../../applicationComponent/component/Select';
import { govIdProofOptions } from '../../../utils/util';
import FileUpload from '../../../applicationComponent/component/FileUpload';
import Input from '../../../applicationComponent/component/Input';





class VerifyDetailsGovProof extends Component {

    constructor(props) {
        super(props)
        this.backPage = this.backPage.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }

    backPage() {
        Actions.verifyDetails();
    }

    onSubmit(values) {

        console.log("-------->" + JSON.stringify(values));
        // let { submitVerifyDetailsGovProof } = this.props;
        //  submitVerifyDetailsGovProof();
        Actions.verifyDetailsReferences();
    };


    render() {
        let { handleSubmit, pristine, submitting } = this.props;
        return (<View style={{ flex: 1 }}>
            <Header backPage={this.backPage} label={"Verify Details"} helpText={"We need a few more details to get started"} />
            <Form>
                <View style={styles.contentBlock}>
                    <View>
                        <Text style={styles.contentHeaderH1}>Identification</Text>
                    </View>
                    <View style={styles.selectBlock}>
                        <Text style={styles.contentHeaderH1}>Upload Govt issued ID Proof</Text>
                        <Field
                            component={Select}
                            name="govIdProof"
                            options={govIdProofOptions}
                        />
                        <View style={{ padding: 1, margin: 15, flexDirection: 'row', justifyContent: 'space-between' }}>

                            <Field name="Front" component={FileUpload} />
                            <Field name="Bank" component={FileUpload} />
                        </View>
                    </View>
                    <View style={styles.selectBlock}>
                        <Text style={styles.contentHeaderH1}>Select Type of Address</Text>
                        <Field
                            component={Select}
                            name="govIdProof"
                            options={govIdProofOptions}
                        />



                        <Field label="Address" name="address" component={Input} />

                    </View>
                </View>

                <SubmitButton label={"NEXT"} handleSubmit={handleSubmit} onSubmit={this.onSubmit} />
            </Form></View>);
    }

}

const validate = (values) => {


    const errors = {};
    if (!values.govIdProof) {
        errors.govIdProof = "Gov Id Proof is required"
    }

    return errors;
};

const form = reduxForm({
    form: 'verifyDetailsGovProof',
    enableReinitialize: true,
    validate
})(VerifyDetailsGovProof)

const mapStateToProps = state => {


    return {
        initialValues: {
            govIdProof: 'Voter Id'
        }

    }
}

const mapDispatchToProps = dispatch => {
    return {
        submitVerifyDetailsGovProof: () => dispatch(saveVerifyDetailsGovProof())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(form);



const styles = StyleSheet.create({
    contentBlock: {
        paddingTop: 20,
        paddingRight: 20
    },
    contentHeaderH1: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '900',
        fontSize: 14,
        lineHeight: 17,
        color: '#333333',
        marginBottom: 30
    },

    selectBlock: {
        paddingBottom: 5,
    },
    selectBlockIonLabel: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 10,
        lineHeight: 12,
        color: '#828282'
    },

    selectBlockIonSelect: {
        marginTop: 10,
        borderRadius: 4,
        paddingTop: 6,
        paddingRight: 10,
        fontWeight: '500',
        fontSize: 12,
        lineHeight: 18,
        fontFamily: 'Montserrat'
    },

    attBlockRect: {
        width: 63,
        height: 60,
        borderRadius: 8,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'

    },

    attBlockIonIcon: {
        color: '#0094FF'
    },

    attBlockIonLabel: {
        fontSize: 8,
        color: '#4F4F4F',
        fontWeight: '500',
        fontFamily: 'Open Sans'
    },


    attBlockAttTxtP: {
        color: '#4F4F4F',
        fontWeight: '500',
        fontSize: 10,
        lineHeight: 18,
        fontFamily: 'Open Sans',
        textAlign: 'left',
        marginLeft: 12,
        marginBottom: 0

    }

});