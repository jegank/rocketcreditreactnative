import React, { Component } from 'react';
import { View, Text, StyleSheet, label, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import Form from '../../../applicationComponent/component/Form';
import { Actions } from 'react-native-router-flux';
import Header from '../../../applicationComponent/component/Header';
import SubmitButton from '../../../applicationComponent/component/SubmitButton';
import { saveVerifyDetailsReferences } from '../../../redux/actions/verify.details.action';



class VerifyDetailsReferences extends Component {

    constructor(props) {
        super(props)
        this.backPage = this.backPage.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }

    backPage() {
        Actions.verifyDetailsGovProof();
    }

    onSubmit() {
        let { saveVerifyDetailsReferences } = this.props;
        saveVerifyDetailsReferences();
        Actions.applicationStatus();
    };


    render() {
        let { handleSubmit } = this.props;
        return (<View style={{ flex: 1 }}>
            <Header backPage={this.backPage} label={"Verify Details"} helpText={"We need a few more details to get started"} />
            <Form>
                <View style={styles.contentBlock}>
                    <View>
                        <Text style={styles.contentHeaderH1}>References</Text>
                    </View>
                </View>
                <SubmitButton label={"FINISH"} handleSubmit={handleSubmit} onSubmit={this.onSubmit} />
            </Form></View>);
    }

}

const form = reduxForm({
    form: 'verifyDetailsReferences'
})(VerifyDetailsReferences)

const mapStateToProps = state => {
    return {

    }
}

const mapDispatchToProps = dispatch => {
    return {
        saveVerifyDetailsReferences: () => dispatch(saveVerifyDetailsReferences())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(form);

const styles = StyleSheet.create({
    contentBlock: {
        paddingTop: 20,
        paddingRight: 20
    },
    contentHeaderH1: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '900',
        fontSize: 14,
        lineHeight: 17,
        color: '#333333',
        marginBottom: 30
    },

    selectBlock: {
        paddingBottom: 5,
    },
    selectBlockIonLabel: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 10,
        lineHeight: 12,
        color: '#828282'
    },

    selectBlockIonSelect: {
        marginTop: 10,
        borderRadius: 4,
        paddingTop: 6,
        paddingRight: 10,
        fontWeight: '500',
        fontSize: 12,
        lineHeight: 18,
        fontFamily: 'Montserrat'
    },

    attBlockRect: {
        width: 63,
        height: 60,
        borderRadius: 8,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'

    },

    attBlockIonIcon: {
        color: '#0094FF'
    },

    attBlockIonLabel: {
        fontSize: 8,
        color: '#4F4F4F',
        fontWeight: '500',
        fontFamily: 'Open Sans'
    },


    attBlockAttTxtP: {
        color: '#4F4F4F',
        fontWeight: '500',
        fontSize: 10,
        lineHeight: 18,
        fontFamily: 'Open Sans',
        textAlign: 'left',
        marginLeft: 12,
        marginBottom: 0

    }

});