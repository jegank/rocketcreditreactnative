import React, { Component } from 'react';
import { View, Text, StyleSheet, label, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import Form from '../../../applicationComponent/component/Form';
import { Actions } from 'react-native-router-flux';
import Header from '../../../applicationComponent/component/Header';
import SubmitButton from '../../../applicationComponent/component/SubmitButton';
import { submitRejectedLoan } from '../../../redux/actions/rejected.action';
import mobileBlack from '../../../applicationComponent/images/mobile_black.png';



class Rejected extends Component {

    constructor(props) {
        super(props)
        this.backPage = this.backPage.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }

    backPage() {
        Actions.applicationStatus();
    }

    onSubmit() {
        let { submitApplicationStatus } = this.props;

        Actions.verifyDetailsReferences();
    };


    render() {
        let { handleSubmit } = this.props;
        return (<View style={{ flex: 1 }}>
            <Header backPage={this.backPage} label={"Application Status"} helpText={"We need a few more details to get started"} />
            <Form>

                <View style="main_bg">
                    <View style={styles.imgBlock}>
                        <Image source={mobileBlack} style={styles.img} />

                    </View>
                    <View style={styles.apContent}>
                        <Text style={styles.apContenth2}>Your application has benn rejected.</Text>
                        <Text style={styles.apContentp}>Please try again after 15 days.</Text>
                    </View>
                    <View style={styles.btnBlock}>
                        <Text>Pay early to reduce intrest rate. Click "Pay Now". Amount will be automatically reduced</Text>
                    </View>
                </View>
                <SubmitButton label={"Close"} handleSubmit={handleSubmit} onSubmit={this.onSubmit} />

            </Form></View>);
    }

}

const form = reduxForm({
    form: 'rejected'
})(Rejected)

const mapStateToProps = state => {
    return {
    }
}

const mapDispatchToProps = dispatch => {
    return {
        submitApplicationStatus: () => dispatch(submitApplicationStatus())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(form);

const styles = StyleSheet.create({
    button: {
        borderRadius: 8,
        backgroundColor: '#EB5757',
        color: '#ffffff',
        fontFamily: 'Open Sans',
        fontSize: 12,
        fontWeight: '500',
        lineHeight: 19,
        marginTop: 15,
        paddingTop: 12,
        paddingRight: 15
    },
    p: {
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 12,
        fontFamily: 'Open Sans',
        lineHeight: 16,
        color: '#4F4F4F'
    },
    imgBlock: {
        textAlign: 'center',
        position: 'relative',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    img: {
        width: 150,
        height: 300
    },
    triBlock: {
        width: 0,
        height: 0,


        marginRight: 10,
        borderBottomLeftRadius: 15,
        position: 'absolute',
        top: 0,
        opacity: 0.8,
        zIndex: 99
    },
    h2: {
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 22,
        fontFamily: 'Montserrat',
        color: '#000000',
        margin: 0
    },
    btnBlock: {
        paddingTop: 0,
        paddingRight: 30,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    apContent: {
        textAlign: 'center',
        marginBottom: 50,
    },
    apContenth2: {
        fontWeight: 'bold',
        fontSize: 15,
        lineHeight: 22,
        color: '#4F4F4F',
        fontFamily: 'Open Sans'
    },
    apContentp: {
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 12,
        fontFamily: 'Open Sans',
        lineHeight: 16,
        color: '#4F4F4F'
    }

}) 