import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import ActionLoanComponent from '../../../applicationComponent/component/ActionLoanMountComponent';
import Slider from '../../../applicationComponent/component/Slider';
import LoanCalculation from '../../../applicationComponent/component/LoanCalculation';
import Coupan from '../../../applicationComponent/component/Coupan';
import Header from '../../../applicationComponent/component/Header';
import Form from '../../../applicationComponent/component/Form';
import { Actions } from 'react-native-router-flux';
import { getTenure, getLoanAmount } from '../../../redux/selectors/take-loan';
import { submitTakeLoan } from '../../../redux/actions/takeloan.action';
import SubmitButton from '../../../applicationComponent/component/SubmitButton';

class TakeLoan extends Component {
    constructor(props) {
        super(props)
        this.backPage = this.backPage.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }

    backPage() {
        Actions.details();
    }

    onSubmit() {
        this.props.submitTakeLoan();
        Actions.verifyDetails();
    };


    render() {
        let { handleSubmit, days, loanAmount } = this.props;
        return (
            <View style={{ flex: 1 }}>
                <Header backPage={this.backPage} label={"Take Personal Loan"} helpText={"Tempor minim id consequat cummoda exercitation cupidatat Lorem ad."} />
                <Form>
                    <View style={styles.inputBlock}>
                        <View style={styles.inputHeader}>
                            <Text style={styles.h2}>Loan Amount</Text>
                            <Field
                                name="loanAmount"
                                component={ActionLoanComponent}
                            />
                            <Field
                                name="tenure"
                                component={Slider}
                                minimumValue={0}
                                maximumValue={100}
                            />
                        </View>
                        <View style={styles.inputHeader}>
                            <Field
                                name="loanSummary"
                                component={LoanCalculation}
                                tenureValue={days}
                                loanAmount={loanAmount}

                            />
                        </View>
                    </View>
                    <Field
                        name="referCoupan"
                        component={Coupan}
                    />
                    <SubmitButton label={"UPDATE"} handleSubmit={handleSubmit} onSubmit={this.onSubmit} />
                </Form>
            </View>
        );
    }
}

const TakeLoanForm = reduxForm({
    form: 'takeLoan'
})(TakeLoan)

const mapStateToProps = state => {
    const dayCount = getTenure(state);
    const days = dayCount ? parseInt(dayCount) : 0;


    const amount = getLoanAmount(state);

    const loanAmount = amount ? amount : 500;

    return {
        initialValues: {
            tenure: 25
        },
        days,
        loanAmount
    }
}

const mapDispatchToProps = dispatch => {
    return {
        submitTakeLoan: () => dispatch(submitTakeLoan)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TakeLoanForm);

//Style
const styles = StyleSheet.create({

    inputBlock: {
        top: 30,
        paddingLeft: 30,
        paddingRight: 30,
    },
    inputHeader: {
        paddingBottom: 30,
        marginRight: 20
    },
    h2: {
        color: 'black',
        fontFamily: 'Montserrat sans-serif',
        fontStyle: 'normal',
        fontWeight: '900',
        fontSize: 14,
        lineHeight: 17,
        textTransform: 'uppercase',
        paddingBottom: 30
    },
    referalCode: {
        marginBottom: 10,
    },
    referalCodeh4: {
        fontSize: 14,
        color: '#565656'
    },
    referalCodeh4Span: {
        color: '#FF783B',
        fontSize: 14,
        fontWeight: '600',

    },
    couponBlock: {
        paddingRight: 10,
        paddingTop: 0
    }



})