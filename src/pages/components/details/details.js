import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import Form from '../../../applicationComponent/component/Form';
import { Field, reduxForm } from 'redux-form';
import Input from '../../../applicationComponent/component/Input';
import { connect } from 'react-redux';
import { radioOption } from '../../../utils/util';
import RadioButtonGroup from '../../../applicationComponent/component/RadioButtonGroup';
import AgreementCheckbox from '../../../applicationComponent/component/AgreementCheckbox';
import Header from '../../../applicationComponent/component/Header';
import SubmitButton from '../../../applicationComponent/component/SubmitButton';
import { Actions } from 'react-native-router-flux';
import { submitUserDetails } from '../../../redux/actions/details.actions';

class Details extends Component {

    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
        this.backPage = this.backPage.bind(this);
    }


    onSubmit = values => {
        let { submitFormData } = this.props;
        submitFormData();
        Actions.takeLoan();
    };

    backPage() {

    }



    render() {
        let { handleSubmit } = this.props;
        return (
            <View style={{ flex: 1 }}>
                <Header backPage={this.backPage} label={"Details"} helpText={"Tempor minim id consequat cummoda exercitation cupidatat Lorem ad."} />
                <Form>
                    <View style={styles.inputBlock}>
                        <View style={styles.inputHeader}>
                            <Text style={styles.h2}>Personal Info</Text>
                            <Field label="Mobile Numer" placeholder="9003158469" name="mobilenumber" component={Input} />
                            <Field label="Full Name" placeholder="jegan" name="fullname" component={Input} />
                            <Field label={"Gender"} name="gender" options={radioOption} component={RadioButtonGroup} />
                            <Field label="Date Of Birth" placeholder="06-11-1991" name="dateofbirth" component={Input} />
                            <Field label="Email" placeholder="jegan00347@gmail.com" name="email" component={Input} />
                            <Field label="Educational Qualification" placeholder="Msc" name="eduqualification" component={Input} />
                            <Field label="Type Of Accomodation" placeholder="Rent" name="typeofaccomidation" component={Input} />
                            <Field label="Pincode" placeholder="600041" name="pincode" component={Input} />
                            <Field label="City" placeholder="chennai" name="city" component={Input} />
                            <Field label="Area" placeholder="chennai" name="area" component={Input} />
                            <Field label="State" placeholder="chennai" name="state" component={Input} />
                        </View>
                    </View>
                    <View style={styles.inputBlock}>
                        <View style={styles.inputHeader}>
                            <Text style={styles.h2}>Professional Info</Text>
                            <Field label="Employment Type" placeholder="IT" name="employmentType" component={Input} />
                            <Field label="Employer Name" placeholder="Dhanusuya Info Tech" name="employerName" component={Input} />
                            <Field label="Working Since" placeholder="2020" name="workingSince" component={Input} />
                            <Field label="Designation" placeholder="IT Analyst" name="designation" component={Input} />
                            <Field label="Net Monthly Salary" placeholder="150000" name="netMonthlySalary" component={Input} />
                        </View>
                    </View>
                    <View style={styles.agreament}>
                        <Field
                            name="accept"
                            component={AgreementCheckbox}
                            label={"I confirm and accept Rocket Credit products. Terms & Conditions and Privacy Policy"}
                        />
                    </View>
                    <SubmitButton label={"UPDATE"} handleSubmit={handleSubmit} onSubmit={this.onSubmit} />
                </Form >

            </View>


        );
    }

}

//redux
const detailsForm = reduxForm({
    form: 'details'
})(Details);

const mapStateToProps = state => {
    return {
        initialValues: {
            mobilenumber: '9003158469'
        }
    };
}

const mapDispatchToProps = dispatch => {
    return {
        submitFormData: () => dispatch(submitUserDetails())
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(detailsForm);



//Style
const styles = StyleSheet.create({

    inputBlock: {
        top: 30,
        paddingLeft: 30,
        paddingRight: 30,
    },
    inputHeader: {
        paddingBottom: 30,
    },
    h2: {
        color: 'black',
        fontFamily: 'Montserrat sans-serif',
        fontStyle: 'normal',
        fontWeight: '900',
        fontSize: 14,
        lineHeight: 17,
        textTransform: 'uppercase',
        paddingBottom: 30
    },
    inputDiv: {
        position: 'relative',
        paddingBottom: 30,
        width: 100
    },
    flotingLabel: {
        position: 'absolute',
        top: 0,
        left: 0,
        color: '#828282',
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 10,
        lineHeight: 12,
        textTransform: 'uppercase'

    },
    agreament: {
        marginBottom: 40,
        marginLeft: 40,
        marginRight: 30
    }



})

