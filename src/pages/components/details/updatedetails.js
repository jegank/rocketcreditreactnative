import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    TouchableOpacity,
    Alert,
} from 'react-native';
import Form from '../../../applicationComponent/component/Form';
import { Field, reduxForm } from 'redux-form';
import Input from '../../../applicationComponent/component/Input';
import SubmitButton from '../../../applicationComponent/component/SubmitButton';


export default class UpdateDetails extends Component<{}> {

    onSubmit = values => {
        Actions.takeloan();
    };

    render() {
        let { handleSubmit } = this.props;
        return (
            <View style={{ flex: 1 }}>
                <Form>

                    <Text> UTextdate Bank Info </Text>
                    <Text>TemTextor minim id consequat commodo exercitation cuTextidatat Lorem ad.</Text>

                    <View style={styles.inputHeader}>
                        {/* <Text style={styles.h2}>Add New Bank Account</Text> */}
                        <Field label="Account Number" placeholder="102110100023956" name="ACCOUNT NUMBER" component={Input} />
                        <Field label="" placeholder="anbk00010211" name="Ifsc Code" component={Input} />
                        <Field label="Account Holeder's Name" placeholder="pavan" name="AccHolderName" component={Input} />
                        <SubmitButton label={"Proceed"} handleSubmit={handleSubmit} onSubmit={this.onSubmit} />
                    </View>
                </Form>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    inputHeader: {
        paddingBottom: 30,
    },
    h2: {
        color: 'black',
        fontFamily: 'Montserrat sans-serif',
        fontStyle: 'normal',
        fontWeight: '900',
        fontSize: 14,
        lineHeight: 17,
        textTransform: 'uppercase',
        paddingBottom: 30
    },
});
