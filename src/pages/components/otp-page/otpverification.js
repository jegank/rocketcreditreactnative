import React, { Component } from 'react';
import { View, Text, StyleSheet, label, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import Form from '../../../applicationComponent/component/Form';
import { Actions } from 'react-native-router-flux';
import Header from '../../../applicationComponent/component/Header';
import SubmitButton from '../../../applicationComponent/component/SubmitButton';
import { saveVerifyDetailsReferences } from '../../../redux/actions/verify.details.action';



class OtpVerificationCompleted extends Component {

    constructor(props) {
        super(props)
        this.backPage = this.backPage.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }

    backPage() {
        Actions.OtpVerificationGovProof();
    }

    onSubmit() {
        let { saveOtpVerificationCompleted } = this.props;
        saveOtpVerificationCompleted();
        Actions.applicationStatus();
    };


    render() {
        let { handleSubmit } = this.props;
        return (<View style={{ flex: 1 }}>
            <Header backPage={this.backPage} label={"Verify Details"} helpText={"We need a few more details to get started"} />
            <Form>
                <SubmitButton label={"FINISH"} handleSubmit={handleSubmit} onSubmit={this.onSubmit} />
            </Form></View>);
    }

}

const form = reduxForm({
    form: 'OtpVerificationCompleted'
})(OtpVerificationCompleted)

const mapStateToProps = state => {
    return {

    }
}

const mapDispatchToProps = dispatch => {
    return {
        saveOtpVerificationCompleted: () => dispatch(saveOtpVerificationCompleted())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(form);