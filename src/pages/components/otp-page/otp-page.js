import React, { Component } from 'react';
import { Text, Image, View, TouchableHighlight, StyleSheet, ImageBackground } from 'react-native';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import Form from '../../../applicationComponent/component/Form';
import mobile from '../../../applicationComponent/images/mobile.png';
import otp from "../../../applicationComponent/images/otp.png";
import SubmitButton from "../../../applicationComponent/component/SubmitButton";
import Input from "../../../applicationComponent/component/Input";
import { submitOtpActions } from "../../../redux/actions/otp.actions";


class OtpPage extends Component {

    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit() {
        let { saveOtpActions } = this.props;
        saveOtpActions();
    }



    render() {
        let { handleSubmit } = this.props;
        return (<View style={{ flex: 1 }}>
            <Form>
                <View style={styles.mainBg}>
                    {/* <ImageBackground source={otp} style={{ height: 50, width: 100 }}> */}
                    <View style={styles.walBlock}>
                        <Image source={mobile} style={styles.img} />
                    </View>
                    <View >
                        <View style={styles.titleHeader}>
                            <Text style={styles.titleHeaderH1}>Verify your Mobile Number</Text>
                        </View>
                        <View style={styles.text}>
                            <Text style={styles.textp}>We will send you an One Time Password on this mobile number</Text>
                        </View>
                        <View style={styles.inputBlock}>

                            <Field label="Enter your Mobile Number" placeholder="9003158469" name="mobilenumber" component={Input} />
                        </View>
                        {/* </ImageBackground> */}
                    </View>
                </View>
                <SubmitButton label={"GET OTP"} handleSubmit={handleSubmit} onSubmit={this.onSubmit} />
            </Form>

        </View >);
    }
}

const mapStateToProps = (state) => {
    return {

    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        saveOtpActions: () => dispatch(submitOtpActions()),
    }
}

const form = reduxForm({
    form: 'otpPage'
})(OtpPage);


export default connect(mapStateToProps, mapDispatchToProps)(form)

const styles = StyleSheet.create({
    mainBg: {
        width: 100,

        //backgroundRepeat: 'no-repeat',
        // backgroundPosition: 'bottom',
        height: 100,
        //backgroundSize: 'cover'
    },
    img: {
        width: 80,
        height: 80
    },
    walBlock: {
        paddingTop: 80,
        textAlign: 'center'

    },
    titleHeader: {
        textAlign: 'center',
        paddingTop: 5
    },
    titleHeaderH1: {
        fontFamily: 'Open Sans',
        fontSize: 16,
        fontWeight: 'bold',
        lineHeight: 22,
        color: '#0094FF'
    },

    text: {
        textAlign: 'center',
        paddingTop: 0,
        paddingRight: 20,
    },
    textp: {
        fontFamily: 'Open Sans',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 12,
        lineHeight: 19,
        color: '#4F4F4F'
    },
    inputBlock: {
        width: 130,
        marginTop: 0,
        marginRight: 20,


    },

    inputBlocktelInput: {
        width: 100,

        backgroundColor: 'transparent',
        // borderBottom: 1,
        paddingTop: 0,
        paddingRight: 12,
        paddingBottom: 8
    },
    inputBlockP: {
        fontSize: 10,
        lineHeight: 14,
        color: '#4F4F4F',
        fontFamily: 'Open Sans'
    }





})
