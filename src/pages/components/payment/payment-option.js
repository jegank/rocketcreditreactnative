import React, { Component } from 'react';
import { View, Text, StyleSheet, label, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import Form from '../../../applicationComponent/component/Form';
import FieldsContainer from '../../../applicationComponent/component/FieldsContainer';
import FieldSet from '../../../applicationComponent/component/FieldSet';
// import FileUpload from '../../../applicationComponent/component/FileUpload';
import Label from '../../../applicationComponent/component/Label';
import { Actions } from 'react-native-router-flux';
import Header from '../../../applicationComponent/component/Header';
import SubmitButton from '../../../applicationComponent/component/SubmitButton';
// import { saveVerifyDetails } from '../../../redux/actions/verify.details.action';



class PaymentOptions extends Component {

    constructor(props) {
        super(props)
        this.backPage = this.backPage.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }

    backPage() {
        Actions.payments();
    }

    onSubmit() {
        Actions.loanClosing();
    };

    render() {

        let { handleSubmit } = this.props;

        return (
            <View style={{ flex: 1 }}>
                <Header backPage={this.backPage} label={"Payment"} helpText={"We need a few more details to get started"} />
                <Form>
                    <FieldSet label="Select Payment Mode" >
                        <FieldsContainer>
                            <View style = {styles.images}>
                         
                        <Image source = {require('../../../applicationComponent/images/qrcode.png')} style = {styles.image} />
                        <Image source = {require('../../../applicationComponent/images/netbank.png')} style = {styles.image} />
                         {/* <Hr style = {styles.lines} /> */}
                        <Image source = {require('../../../applicationComponent/images/whiteimps.png')} style = {styles.image} />
                        </View>
                            <FieldSet label="Pay early to for reduced intrest rate. Click 'Pay Now'. Amount will be automatically reduced." >
                            </FieldSet>
                        </FieldsContainer>
                        <FieldsContainer>
                            <View style = {styles.main}>
                                <View style = {styles.sub }>
                                    <View style = {styles.subs}>
                                        <Text style = {styles.texts}>Benificiary Name</Text>
                                        <Text>acHolderNam</Text>
                                    </View>
                                    <View style = {styles.subs}>
                                        <Text style = {styles.texts}>Bank Name</Text>
                                        <Text>Yes Bank</Text>
                                    </View>
                                    <View style = {styles.subs}>
                                        <Text style = {styles.texts}>A/C Number</Text>
                                        <Text>accountNumbe</Text>
                                    </View>
                                    <View style = {styles.subs}>
                                        <Text style = {styles.texts}>IFSC Code</Text>
                                         <Text>ifscCod</Text>
                                    </View>
                                    <View style = {styles.subs}>
                                        <Text style = {styles.texts}>Account Type</Text>
                                        <Text>Current</Text>
                                    </View>
                                </View>
                            </View>
                        </FieldsContainer>
                        <FieldsContainer>
                            <SubmitButton  label={"UPLOAD PROOF"} handleSubmit={handleSubmit} onSubmit={this.onSubmit} />
                        </FieldsContainer>
                    </FieldSet>
                </Form >
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        
    }
}
const form = reduxForm({
    form: 'paymentOptions'
})(PaymentOptions)


const mapDispatchToProps = (dispatch) => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(form)

const styles = StyleSheet.create({
    images:{
        padding: 1,
        margin: 15,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    image:{
        width: 70,
        height: 70
    },
    // lines: {
    //     backgroundColor: "blue",
    //     height: 4
        // minHeight: 75,
        // padding: (0,1),
        // borderRight: (0.4,solid, '#4F4F4F')
    // },
    main: {
        padding: ('20px',0)
    },
    sub: {
        backgroundColor: "#F2F2F2",
        borderRadius: 4,
        minHeight: 200,
        padding: (10,15)
    },
    subs: {
        display: "flex",
        justifyContent: "space-between",
        flexDirection: 'row',
        fontFamily: ('Roboto', 'sans-serif'),
        color: '#333333',
        fontWeight: 'bold',
        fontSize: 14,
        lineHeight: 16,
        margin: ('10px', 0),
        padding: 1,
        margin: 15
        

    },
    texts: {
        color: "#4F4F4F"
    }
});