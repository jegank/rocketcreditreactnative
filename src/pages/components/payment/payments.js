import React, { Component } from 'react';
import { View, Text, StyleSheet, label, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import Form from '../../../applicationComponent/component/Form';
import FieldsContainer from '../../../applicationComponent/component/FieldsContainer';
import FieldSet from '../../../applicationComponent/component/FieldSet';
// import FileUpload from '../../../applicationComponent/component/FileUpload';
import Label from '../../../applicationComponent/component/Label';
import { Actions } from 'react-native-router-flux';
import Header from '../../../applicationComponent/component/Header';
import SubmitButton from '../../../applicationComponent/component/SubmitButton';
// import { saveVerifyDetails } from '../../../redux/actions/verify.details.action';



class Payments extends Component {

    constructor(props) {
        super(props)
        this.backPage = this.backPage.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }

    backPage() {
        Actions.home();
    }

    onSubmit() {
        Actions.paymentOptions();
    };

    render() {

        let { handleSubmit } = this.props;

        return (
            <View style={{ flex: 1 }}>
                <Header backPage={this.backPage} label={"Payment"} helpText={"We need a few more details to get started"} />
                <Form>
                    <FieldSet label="Select Payment Mode" >
                        <FieldsContainer>
                            <View style={{ padding: 1, margin: 15, flexDirection: 'row', justifyContent: 'space-between' }}>

                                <Image source={require('../../../applicationComponent/images/qrcode.png')} style={{
                                    width: 70, height: 70
                                }} />
                                <Image source={require('../../../applicationComponent/images/netbank.png')} style={{
                                    width: 70, height: 70
                                }} />
                                <Image source={require('../../../applicationComponent/images/whiteimps.png')} style={{
                                    width: 70, height: 70
                                }} />
                            </View>
                            <FieldSet label="Pay early to for reduced intrest rate. Click 'Pay Now'. Amount will be automatically reduced." >
                            </FieldSet>
                        </FieldsContainer>

                        <FieldsContainer>
                            <SubmitButton label={"PAY NOW"} handleSubmit={handleSubmit} onSubmit={this.onSubmit} />
                        </FieldsContainer>
                    </FieldSet>
                </Form ></View>);
    }
}

const mapStateToProps = (state) => {
    return {

    }
}
const form = reduxForm({
    form: 'payments'
})(Payments)


const mapDispatchToProps = (dispatch) => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(form)

