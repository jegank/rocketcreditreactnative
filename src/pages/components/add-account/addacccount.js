import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    TouchableOpacity,
    Alert,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Form from '../../../applicationComponent/component/Form';
import { Field, reduxForm } from 'redux-form';

export default class AddAccount extends Component<{}> {
    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit = values => {
        Actions.updatedetails();
    }
 render(){
    let { handleSubmit } = this.props;
    return (
        <View style={styles.container}>
            <View>
                <Text style={styles.buttonText}>INSTANT CREDIT LINE</Text>
            </View>

            <View>
                <Text>UPON verification we credit your account the same day </Text>
            </View>
            <TouchableOpacity
                style={styles.button}
                onPress={this.onSubmit}
            >
                <Text style={styles.buttonText}>ADD BANK ACCOUNT</Text>
            </TouchableOpacity>
        </View>
    );
 }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
    },
    button: {
        width: 300,
        backgroundColor: 'red',
        borderRadius: 25,
        marginVertical: 25,
        paddingVertical: 13,

    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center',
    },
    signupText: {
        color: 'rgba(255,255,255,0.6)',
        fontSize: 16
    }
});