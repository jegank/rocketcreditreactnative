import React, { Component } from 'react';
import { View, Text, StyleSheet, label, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import Form from '../../../applicationComponent/component/Form';
import { Actions } from 'react-native-router-flux';
import Header from '../../../applicationComponent/component/Header';
import SubmitButton from '../../../applicationComponent/component/SubmitButton';
import { submitApplicationStatus } from '../../../redux/actions/application.status.action';



class ApplicationStatus extends Component {

    constructor(props) {
        super(props)
        this.backPage = this.backPage.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

    }

    backPage() {
        Actions.verifyDetailsReferences();
    }

    onSubmit() {
        let { submitApplicationStatus } = this.props;
        submitApplicationStatus();
        Actions.rejected();
    };


    render() {
        let { handleSubmit } = this.props;
        return (<View style={{ flex: 1 }}>
            <Header backPage={this.backPage} label={"Application Status"} helpText={"We need a few more details to get started"} />
            <Form>
                <View style={styles.contentBlock}>
                    <View style={styles.header_txxt_h1}>
                        <Text style={styles.h1}>Loan Status</Text>
                    </View>
                    <View style={styles.headerTxt1}>
                        <Text style={styles.h4}>Loan Amount</Text>
                        <Text style={styles.h2}>₹ 10000</Text>
                    </View>
                    <View style={styles.headerTxt1}>
                        <Text style={styles.h4}>Status</Text>
                        <Text style={styles.headerTxt1H3Icon}>
                            Progress
                        </Text>
                    </View>
                    <View style="due_date">
                        <Text style={styles.h4}>Due Date</Text>
                        <Text style={styles.h3}>10-2-2399</Text>
                    </View>
                    <View style="header_txt1">
                        <Text style={styles.h4}>Amount Payable</Text>
                        <Text style={styles.h2}>₹ 20000</Text>
                    </View>
                    <View style="loan_notification">
                        <Text style={styles.h5}>You will be notified when your loan is approved</Text>

                    </View>
                </View>

                <SubmitButton label={"CANCEL"} handleSubmit={handleSubmit} onSubmit={this.onSubmit} />
            </Form></View >);
    }

}

const form = reduxForm({
    form: 'applicationStatus'
})(ApplicationStatus)

const mapStateToProps = state => {
    return {

    }
}

const mapDispatchToProps = dispatch => {
    return {
        submitApplicationStatus: () => dispatch(submitApplicationStatus())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(form);

const styles = StyleSheet.create({
    h5: {

    },
    h4: {
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 10,
        color: '#828282',
        fontFamily: 'Montserrat'
    },
    h3: {

    },
    h2: {
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 22,
        fontFamily: 'Montserrat',
        color: '#000000',
        margin: 0
    },
    h1: {

    },
    contentBlock: {
        paddingRight: 15,
        paddingTop: 30
    },

    header_txxt_h1: {
        fontFamily: 'Montserrat',
        fontStyle: 'normal',
        fontWeight: '600',
        fontSize: 14,
        lineHeight: 17,
        color: '#333333',
        margin: 0
    },
    headerTxt1: {
        color: '#F2994A',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 22,
        margin: 0,
        fontFamily: 'Montserrat',
        display: 'flex',
        alignItems: 'center'
    },
    headerTxt1H3Icon: {

        width: 15,
        height: 15,
        color: '#F2994A',
        marginRight: 4
    }

}) 