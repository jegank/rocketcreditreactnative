import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import Form from '../../applicationComponent/component/Form';
import FieldsContainer from '../../applicationComponent/component/FieldsContainer';
import FieldSet from '../../applicationComponent/component/FieldSet';
import FormGroup from '../../applicationComponent/component/FormGroup';
import Label from '../../applicationComponent/component/Label';
import Input from '../../applicationComponent/component/Input';
import CheckboxGroup from '../../applicationComponent/component/CheckBoxGroup';
import RadioButtonGroup from '../../applicationComponent/component/RadioButtonGroup';
import SelectBox from '../../applicationComponent/component/Select';
//import FileUpload from './component/FileUpload';
import Header from '../../applicationComponent/component/Header';
import AgreementCheckbox from '../../applicationComponent/component/AgreementCheckbox';
import Slider from '../../applicationComponent/component/Slider';
import ActionLoanAmoutComponent from '../../applicationComponent/component/ActionLoanMountComponent';






class Index extends Component {

    constructor(props) {
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit = (values) => {

    }

    render() {
        const { handleSubmit, loginUser } = this.props;

        return (<Form>


            <Field label="UserName" placeholder="john" name="userName" component={Input} />




            <TouchableOpacity onPress={handleSubmit(this.onSubmit)}>
                <Text>Login</Text>
            </TouchableOpacity>


        </Form>);
    }

}

const validate = (values) => {
    console.log("values-->", values)

    const errors = {};
    if (!values.userName) {
        errors.userName = "Name is required"
    }
    if (!values.email) {
        errors.email = "Email is required"
    }
    if (!values.password) {
        errors.password = "password is required"
    }
    return errors;
};

const mapStateToProps = (state) => {
    //console.log("state---->" + JSON.stringify(state));
    return {
        initialValues: {
            amenities: {
                pool: true,
                basketballCourt: true,
                yard: false
            },
            userName: "jegan",
            gender: "male",
            vegies: 12,
            agreementCheckBox: false
        }

    }
}

const form = reduxForm({
    form: 'home',
    validate
})(Index)

export default connect(mapStateToProps)(form);

