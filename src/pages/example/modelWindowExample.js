import React, { Component } from 'react';
import { View, Text, StyleSheet, label, TouchableOpacity, Alert, Button } from 'react-native';
import ModalWindow from '../../applicationComponent/component/ModalWindow';
import Header from '../../applicationComponent/component/Header';
import { Actions } from 'react-native-router-flux';


export default class ExampleModalWindow extends Component {

    constructor(props) {
        super(props)
        this.okButton = this.okButton.bind(this);
        this.showAlert = this.showAlert.bind(this);
        this.content = this.content.bind(this);
        this.backPage = this.backPage.bind(this);

    }

    componentDidMount() {
        this.setState({ alertVisibility: true });
    }

    backPage() {
        Actions.verifyDetailsReferences();
    }



    okButton = () => {
        Alert.alert(

            'Alert Title',
            'My Alert Msg',
            [
                { text: 'Ask me later', onPress: () => console.log('Ask me later pressed') },
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'OK', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false });
    }

    state = {
        alertVisibility: false
    };

    showAlert(alertVisibility) {
        this.setState({ alertVisibility });
    }

    content(props) {
        return (<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <View style={styles.Alert_Main_View}>
                <Text style={styles.Alert_Title}>Custom Alert Dialog Title.</Text>
                <View style={{ width: '100%', height: 2, backgroundColor: '#fff' }} />
                <Text style={styles.Alert_Message}> Are You Sure(Alert Dialog Message). </Text>
                <View style={{ width: '100%', height: 1, backgroundColor: '#fff' }} />
                <View style={{ flexDirection: 'row', height: '30%' }}>
                    <TouchableOpacity
                        style={styles.buttonStyle}
                        onPress={props.okButton}
                        activeOpacity={0.7}
                    >
                        <Text style={styles.TextStyle}> OK </Text>
                    </TouchableOpacity>
                    <View style={{ width: 1, height: '100%', backgroundColor: '#fff' }} />
                    <TouchableOpacity
                        style={styles.buttonStyle}
                        onPress={() => { props.showAlert(!props.alertVisibility) }}
                        activeOpacity={0.7}
                    >
                        <Text style={styles.TextStyle}> CANCEL </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>)
    }

    render() {
        return (<View style={{ flex: 1 }}>
            <Header backPage={this.backPage} label={"Modal Window Eamples"} helpText={"We need a few more details to get started"} />
            <ModalWindow alertVisibility={this.state.alertVisibility} showAlert={this.showAlert}>
                {this.content({
                    alertVisibility: this.state.alertVisibility,
                    showAlert: (value) => this.showAlert(value),
                    okButton: () => this.okButton()
                })}
            </ModalWindow>

        </View>);
    }
}

const styles = StyleSheet.create({

    Alert_Main_View: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "#009688",
        height: 200,
        width: '90%',
        borderWidth: 1,
        borderColor: '#fff',
        borderRadius: 7,
    },

    Alert_Title: {
        fontSize: 25,
        color: "#fff",
        textAlign: 'center',
        padding: 10,
        height: '28%'
    },

    Alert_Message: {
        fontSize: 22,
        color: "#fff",
        textAlign: 'center',
        padding: 10,
        height: '42%'
    },

    buttonStyle: {
        width: '50%',
        height: '50%',
        justifyContent: 'center',
        alignItems: 'center'
    },

    TextStyle: {
        color: '#fff',
        textAlign: 'center',
        fontSize: 22,
        marginTop: -5
    }

});