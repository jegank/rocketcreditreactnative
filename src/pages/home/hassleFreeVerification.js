/* eslint-disable prettier/prettier */
/* eslint-disable no-unused-vars */
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    TouchableOpacity,
    Alert,
} from 'react-native';
import { Actions } from 'react-native-router-flux';


export default class HassleFreeVerification extends Component<{}> {

    onSubmit = values => {
        Actions.details();
    };

    render() {
        return (
            <View style={styles.container}>
                <View>
                    <Text style={styles.buttonText}>HASSLE-FREE VERIFICATION</Text>
                </View>

                <View>
                    <Text>Get yourself verified based upon your spacial profile and other documents you provide for verfication in a hassle free manner</Text>
                </View>
                <TouchableOpacity
                    style={styles.button}
                    onPress={this.onSubmit}
                >
                    <Text style={styles.buttonText}>Next</Text>
                </TouchableOpacity>
            </View>
        );
    }
}





const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
    },
    button: {
        width: 300,
        backgroundColor: 'red',
        borderRadius: 25,
        marginVertical: 25,
        paddingVertical: 13,

    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center',
    },
    signupText: {
        color: 'rgba(255,255,255,0.6)',
        fontSize: 16
    }
});


