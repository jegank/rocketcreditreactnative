import { fetchApi } from "../service/api";
export const actionVerifyDetailsSuccess = 'action/VERIFY_DETAILS_SUCCESS';
export const actionVerifyDetailsFail = 'action/VERIFY_DETAILS_FAIL';

export const verifyDetailsSuccess = (payLoad) => {
    return {
        type: actionVerifyDetailsSuccess,
        payLoad
    }
}

export const verifyDetailsFail = (payLoad) => {
    return {
        type: actionVerifyDetailsFail,
        payLoad
    }
}

export const saveVerifyDetails = () => {
    return async (dispath, getState) => {
        const { authReducer: { authData: { token, userInfo: { id } } }, form: { verifyDetails: values } } = getState();
        const { payslipOne, payslipTwo, payslipThree, bankStatementOne, bankStatementTwo, bankStatementThree } = values && values.values;
        let playSlip = [payslipOne, payslipTwo, payslipThree];
        let bankStatments = [bankStatementOne, bankStatementTwo, bankStatementThree];


        let fileName = bankStatementOne.fileName;
        let base64Image = bankStatementOne.data;


        console.log("---->", bankStatementOne);

        // let data = new FormData();
        // data.append("user_id", id);
        // bankStatments.forEach((item, i) => {
        //     data.append('bankStatments[]', {
        //         uri: item.uri,
        //         type: item.png,
        //         name: item.fileName,
        //     });
        // });
        // playSlip.forEach((item, i) => {
        //     data.append('playSlip[]', {
        //         uri: item.uri,
        //         type: item.png,
        //         name: item.fileName,
        //     });
        // })
        let data = {
            fileName,
            base64Image
        }

        //console.log("--->", data)createFile
        // const response = await fetchApi('/uploadPaySlip/savepaySlip', 'POST', data, 200, token);
        const response = await fetchApi('/api/uploadpayslip', 'POST', data, 200, token);
        console.log("resr===>" + JSON.stringify(response));
    }
}

export const saveVerifyDetailsGovProof = () => {
    return async (dispath, getState) => {
        // service
    }
}

export const saveVerifyDetailsReferences = () => {
    return async (dispath, getState) => {

    }
}