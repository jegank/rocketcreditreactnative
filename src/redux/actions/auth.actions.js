import { fetchApi } from "../service/api";
export const actionLoginuserSuccess = 'action/LOGIN_USER_SUCCESS';
export const actionLoginuserFail = 'action/LOGIN_USER_FAIL';


export const loginUserSuccess = (payLoad) => {
    return {
        type: actionLoginuserSuccess,
        payLoad
    }
}

export const loginUserFail = () => {
    return {
        type: actionLoginuserFail,
    }
}

export const loginUser = (payLoad) => {
    return async dispatch => {
        dispatch(loginUserSuccess(payLoad));
    }
}








// export const logoutUser = () => {
//     return async (dispatch, getState) => {
//         const state = getState();
//         try {
//             const {
//                 authReducer: {
//                     authData: { token },
//                 },
//             } = state;
//             console.log(token);
//             const response = await fetchApi(
//                 '/user/logout',
//                 'DELETE',
//                 null,
//                 200,
//                 token
//             );
//             console.log(response);
//             dispatch({
//                 type: "USER_LOGGED_OUT_SUCCESS"
//             });
//         } catch (e) {
//             console.log(e);
//         }
//     };
// };

// export const createNewUser = payload => {
//     return async dispatch => {
//         try {
//             dispatch({
//                 type: "CREATE_USER_LOADING"
//             });
//             const response = await fetchApi("/register", "POST", payload, 200);

//             if (response.success) {
//                 dispatch({
//                     type: "CREAT_USER_SUCCESS"
//                 });
//                 dispatch({
//                     type: "AUTH_USER_SUCCESS",
//                     token: response.token
//                 });
//                 dispatch({
//                     type: "GET_USER_SUCCESS",
//                     payload: response.responseBody
//                 });

//                 return response;
//             } else {
//                 throw response;
//             }
//         } catch (error) {
//             dispatch({
//                 type: "CREAT_USER_FAIL",
//                 payload: error.responseBody
//             });
//             return error;
//         }
//     };
// };
