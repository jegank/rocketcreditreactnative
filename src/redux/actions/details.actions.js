import { fetchApi } from "../service/api";
export const actionUserDetailsSuccess = 'action/USER_DETAILS_SUCCESS';
export const actionUserDetailsFail = 'action/USER_DETAILS__FAIL';

export const userDetailsSuccess = (payLoad) => {
    return {
        type: actionUserDetailsSuccess,
        payLoad
    }
}

export const userDetailsFail = (payLoad) => {
    return {
        type: actionUserDetailsFail,
        payLoad
    }
}

export const submitUserDetails = () => {
    return async (dispath, getState) => {
        const { authReducer: { authData: { token, userInfo: { id } } }, form: { details: values } } = getState();
        const loginUserid = id;
        const detailsFormValues = values && values.values;
        let body = { ...detailsFormValues, loginUserid };
        const response = await fetchApi('/personlInfo/savePersonalInfo', 'POST', body, 200, token);
        console.log("----------------->" + response);
    }
}

