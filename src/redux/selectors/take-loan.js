import { formValueSelector } from 'redux-form';
const selector = formValueSelector('takeLoan');

export const getTenure = (state) => {
    return selector(state, 'tenure');
}

export const getLoanAmount = (state) => {
    return selector(state, 'loanAmount');
}


