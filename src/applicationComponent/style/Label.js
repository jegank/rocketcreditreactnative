const Label = {
    color: '#bfc2c9',
    fontSize: 12,
    stackedHeight: 40
}

export default Label;