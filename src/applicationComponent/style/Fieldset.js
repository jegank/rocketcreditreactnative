const Fieldset = {

    borderBottomColor: '#ddd',
    labelColor: '#909090',
    labelSize: 9,
    labelWeight: 700,
    labelHeight: 25,
    paddingTop: 12,
    paddingBottom: 12,
    paddingLeft: 8,
    paddingRight: 8,
}
export default Fieldset;


