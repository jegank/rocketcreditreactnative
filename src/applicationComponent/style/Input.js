const theme = {
    Input: {
        color: '#313131',
    },

    BaseInput: {
        placeholderColor: '#c9c9c9',
        fontSize: 12,
        lineHeight: 18
    }
}

export default theme;