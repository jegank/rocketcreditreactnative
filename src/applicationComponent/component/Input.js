import PropTypes from "prop-types";
import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import { Input } from 'react-native-elements';

class InputText extends Component<{}> {

    state = {
        value: ""
    }

    componentDidMount() {

        this.setState({
            value: this.props.value
        });
        this.input = React.createRef();
    }

    onChangeText = (value) => {
        this.setState({
            value
        }, () => {
            this.props.input.onChange(value);
        })
    }

    render() {
        const { meta: { touched, error }, input: { onChange, ...restInput }, placeholder, secureTextEntry, keyboardType, maxLength, onChangeText, label, onSubmitEditing } = this.props;
        return (
            <View style={style.inputHeader}>
                <Input
                    label={label}
                    placeholder={placeholder}
                    errorStyle={{ color: 'red' }}
                    errorMessage={touched && error}
                    onChangeText={this.onChangeText}
                    {...restInput}
                />
            </View>
        );
    }
}
const propTypes = {
    label: PropTypes.string
};



InputText.propTypes = propTypes;

export default InputText;

const style = StyleSheet.create({
    inputHeader: {
        paddingBottom: 30
    }
});





