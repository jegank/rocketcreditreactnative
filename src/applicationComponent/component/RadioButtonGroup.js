import * as React from "react";
import { TouchableHighlight, View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import checkbox from '../images/checkbox.png';
import unCheckbox from '../images/un-checkbox.png';


// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//     },
//     image: {
//         width: 20,
//         height: 20,
//     },
//     logoText: {
//         marginVertical: 15,
//         fontSize: 18,
//         color: 'rgba(255, 255, 255, 0.7)'
//     }
// });


class Radio extends React.Component {

    handlePress = (name) => {
        let { onChangeState } = this.props;
        onChangeState(name);
    }



    render() {
        const { checked, label, name } = this.props;
        return (<TouchableOpacity onPress={() => {
            this.handlePress(name);
        }}>

            <View style={{ marginLeft: 30 }}>

                {checked ? <Image source={checkbox} style={styles.image} /> :
                    <Image source={unCheckbox} style={styles.image} />
                }
                <View><Text>{label}</Text></View>
            </View>

        </TouchableOpacity>);
    }


}

export default class RadioButtonGroup extends React.Component {

    constructor(props) {
        super(props);
        this.onChangeState = this.onChangeState.bind(this);
    }

    componentDidMount() {
        const { options } = this.props;
        this.setState({ options });
    }

    state = {
        options: []
    }

    onChangeState(selectedValue) {
        const { input: { value, onChange }, options } = this.props;
        onChange(selectedValue);
    }

    render() {
        const { input: { value }, label } = this.props;
        return (
            <View style={styles.inputDiv}>
                <Text style={styles.flotingLabel}>{label}</Text>
                {
                    this.state.options.map(radio =>
                        <Radio
                            key={radio.label}
                            label={radio.label}
                            name={radio.name}
                            onChangeState={this.onChangeState}
                            checked={radio.name === value} />
                    )
                }
            </View>

        );
    }

}

const styles = StyleSheet.create({
    inputDiv: {

        flexDirection: 'row',
        paddingBottom: 30,
        marginLeft: 10,
        flex: 1

    },
    flotingLabel: {
        color: '#828282',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 15,
        textTransform: 'uppercase'
    },
    image: {
        width: 25,
        height: 25,
    },
});