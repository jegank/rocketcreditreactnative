import React, { Component } from 'react';
import { View, Text } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';

export default class SelectBox extends Component {

    constructor(props) {
        super(props);
        this.onChangeState = this.onChangeState.bind(this);
    }

    state = {
        value: ""
    }

    onChangeState(selectedValue) {
        const { input: { onChange } } = this.props;
        this.setState({ value: selectedValue });
        onChange(selectedValue);

    }

    componentDidMount() {
        const { input: { value } } = this.props;
        this.setState({ value: value });
    }


    render() {
        const { meta: { touched, error }, input: { onChange, ...restInput }, options } = this.props;

        //  console.log("error-->" + touched);


        return (
            <View style={{ flex: 1 }}>
                <RNPickerSelect
                    value={this.state.value}
                    onValueChange={(data) => { this.onChangeState(data) }}
                    items={options}

                />

                {touched && error && <Text style={{ color: 'red' }}>{error}</Text>}

            </View>
        )

    }
}