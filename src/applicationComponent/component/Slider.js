import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import Slider from 'react-native-slider';

var styles = StyleSheet.create({
    container: {
        flex: 1,
        marginLeft: 10,
        marginRight: 30,
        justifyContent: 'center',
        marginBottom: 30
    }

});

export default class Sliderbox extends Component {

    state = {
        value: 0
    }

    componentDidMount() {
        const { input: { value } } = this.props;
        this.setState({ value });
    }

    onValueChange(value) {
        const { input: { onChange } } = this.props;
        this.setState({ value });
        onChange(value);
    }

    render() {
        let { minimumValue, maximumValue } = this.props;
        return (<View style={styles.container}>
            <View>
                <Slider
                    value={this.state.value}
                    onValueChange={(value) => { this.onValueChange(value) }}
                    minimumValue={minimumValue}
                    maximumValue={maximumValue} />
            </View>
            <View>
                <Text>Timeperiod: {parseInt(this.state.value)} days</Text>
            </View>
        </View>);
    }

}