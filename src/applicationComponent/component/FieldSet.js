import React from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import defaultTheme from '../style/Fieldset'

const FieldsetLabelText = styled.Text`
  color: ${props => props.theme.labelColor};
  fontSize: ${props => props.theme.labelSize};
  fontWeight: ${props => props.theme.labelWeight};
  height: ${props => props.theme.labelHeight};
`

FieldsetLabelText.defaultProps = {
    theme: defaultTheme
}

const FieldsetLabel = props => <View><FieldsetLabelText>{props.children}</FieldsetLabelText></View>

const FieldsetWrapper = styled.View`
  borderBottomColor: ${props => props.theme.borderBottomColor};
 
  paddingTop : ${props => props.theme.paddingTop};
  paddingRight : ${props => props.theme.paddingRight};
  paddingBottom : ${props => props.theme.paddingBottom};
  paddingLeft : ${props => props.theme.paddingLeft};
`

FieldsetWrapper.defaultProps = {
    theme: defaultTheme
}


const FieldsetFormWrapper = styled.View`

`

const Fieldset = props => {
    const { children, label, last, theme } = props

    return (
        <FieldsetWrapper last={last} theme={theme}>
            {label && <FieldsetLabel>{label.toUpperCase()}</FieldsetLabel>}
            <FieldsetFormWrapper>
                {children}
            </FieldsetFormWrapper>
        </FieldsetWrapper>
    )
}

Fieldset.propTypes = {
    last: PropTypes.bool,
    label: PropTypes.string
}

Fieldset.defaultProps = {
    componentName: 'Fieldset',
    last: false,
    label: false,
    theme: defaultTheme
}

export default Fieldset;