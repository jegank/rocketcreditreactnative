import * as React from "react";
import { TouchableHighlight, View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import checkbox from '../images/checkbox.png';
import unCheckbox from '../images/un-checkbox.png';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    image: {
        width: 20,
        height: 20,
    },
    logoText: {
        marginVertical: 15,
        fontSize: 18,
        color: 'rgba(255, 255, 255, 0.7)'
    }
});

class CheckBox extends React.Component {

    handlePress = (value) => {
        let { onChange } = this.props;
        onChange(!value);
    }



    render() {
        const { checked, label } = this.props;
        return (<TouchableOpacity onPress={() => {
            this.handlePress(checked);
        }}>

            <View style={{ flex: 1, flexDirection: 'row' }}>

                {checked ? <Image source={checkbox} style={styles.image} /> :
                    <Image source={unCheckbox} style={styles.image} />
                }
                <Text>{label}</Text>
            </View>

        </TouchableOpacity>);
    }


}


export default class AgreementCheckbox extends React.Component {

    constructor(props) {
        super(props);
        this.onChangeState = this.onChangeState.bind(this);
    }


    onChangeState(selectedValue) {
        const { input: { onChange } } = this.props;
        onChange(selectedValue);
    }



    render() {
        const { input: { value }, label } = this.props;
        return (
            <CheckBox
                label={label}
                checked={value}
                onChange={this.onChangeState}
            />
        );
    }
} 