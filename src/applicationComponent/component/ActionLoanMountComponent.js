import React, { Component } from "react";
import { View, Text, TouchableOpacity, Image, StyleSheet, Button } from 'react-native';
import increamentIcon from '../images/plus-icon.png';
import decreamentIcon from '../images/minus-icon.png';
import styled from 'styled-components/native';
import defaultTheme from '../style/Fieldset';
import Icon from 'react-native-vector-icons/FontAwesome';




const FieldsetLabelText = styled.Text`
  color: ${props => "blue"};
  fontSize: ${props => props.theme.labelSize};
  fontWeight: ${props => "800"};
  height: ${props => 100};
`


FieldsetLabelText.defaultProps = {
    theme: defaultTheme
}


class LabelWithAmount extends Component {

    onPressLearnMore(value) {
        let { calc } = this.props;
        calc(value);
    }
    //{color: selectedValue === value ? "red" : "black"} 
    render() {
        let { collectionAmount, selectedValue } = this.props;
        return (<View style={styles.btnBlock} >{
            collectionAmount.map(value => (<TouchableOpacity onPress={() => {
                this.onPressLearnMore(value);
            }}>
                {
                    <Text style={styles.ionChip(selectedValue === value)}>{value}</Text>
                }

            </TouchableOpacity>))
        }</View>);
    }
}


export default class ActionLoanMountComp extends Component {

    constructor(props) {
        super(props);
        this.increament = this.increament.bind(this);
        this.decreament = this.decreament.bind(this);
        this.calc = this.calc.bind(this);
    }

    componentDidMount() {
        this.calc(this.state.value);
    }


    state = {
        value: 2000,
        rangeAmount: []
    }



    calc(input) {
        const array = [];
        var findRange;
        var min;
        var max = 50000;
        if (10000 < input) {
            findRange = 8;
        } else if (5000 < input) {
            findRange = 4;
        }

        if (findRange >= 4) {
            min = input - 4 * 500;
        } else {
            min = 500;
        }
        if (min <= max) {
            array.push(min);
        }

        for (var k = 0; k < 8; k++) {
            if (min < max) {
                min = min + 500;
                array.push(min);
            }
        }
        const { input: { onChange } } = this.props;
        onChange(input);
        this.setState({ rangeAmount: array })
    }

    increament(value) {
        if (value < 50000) {
            this.calc(value + 500)
        }

    }

    decreament(value) {
        if (1000 <= value) {
            this.calc(value - 500)
        }
    }
    //600
    render() {
        const { input: { value, onChange } } = this.props;

        return (
            <View >
                <View style={styles.inputDiv}>
                    <TouchableOpacity onPress={() => {
                        this.decreament(value);
                    }}>
                        <Image source={decreamentIcon} style={styles.image} />
                    </TouchableOpacity>
                    <Text style={styles.amountTxt}>{value}</Text>
                    <TouchableOpacity onPress={() => {
                        this.increament(value);
                    }}>
                        <Image source={increamentIcon} style={styles.image} />
                    </TouchableOpacity>
                </View>
                <LabelWithAmount selectedValue={value} calc={this.calc} collectionAmount={this.state.rangeAmount} />

            </View>);
    }
}

const styles = StyleSheet.create({

    inputDiv: {
        flexDirection: 'row',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 20
    },
    flotingLabel: {
        color: '#828282',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 15,
        textTransform: 'uppercase'
    },
    image: {
        width: 30,
        height: 30,
    },
    amountTxt: {
        color: '#0094FF',
        fontSize: 25,
        lineHeight: 42,
        fontFamily: 'Monteserrat',
        fontWeight: 'bold'
    },
    ionChip: (selectedValue) => ({
        width: 60,
        margin: 3,
        borderRadius: 6,
        borderWidth: 2,
        textAlign: 'center',
        borderColor: "#6e1809",
        backgroundColor: selectedValue ? '#0094FF' : 'transparent',
        color: selectedValue ? 'white' : '#565656',
        fontSize: 12,
        lineHeight: 16,
        fontFamily: 'Open Sans sans-serif',
        marginBottom: 20,

    }),
    btnBlock: {
        //  flexDirection: 'row',
        display: 'flex',

        justifyContent: 'space-between',
        /* You can set flex-wrap and
           flex-direction individually */
        flexDirection: 'row',
        flexWrap: 'wrap',


        paddingTop: 30,
        paddingLeft: 30,
        paddingRight: 30,
        paddingBottom: 30,


    }
    // &:hover {
    //     --background: #0194FE;
    //     border: 1px solid #0194FE;
    //     color: #FFFFFF;
    // }

    // +ion-chip {
    //     margin-left: 6px;
    // }


});