import React from 'react'
import { View, TextInput } from 'react-native'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'
import _ from 'lodash'
import defaultTheme from '../style/FormGroup';

const calculateHeight = (props) => {
    let height = props.theme.height

    if (props.multiline) {
        height = props.theme.height * props.numberOfLines
    }

    if (!props.inlineLabel) {
        height += props.theme.Label.stackedHeight
    }
    return (height)
}

const FormGroupWrapper = styled.View`
  align-items: ${props => props.inlineLabel ? 'center' : 'stretch'};
  border-color: ${props => props.error ? props.theme.errorBorderColor : props.theme.borderColor};
  border-radius: ${props => props.theme.borderRadius};
  border-style: ${props => props.theme.borderStyle};
  border-width: ${props => props.border ? props.theme.borderWidth : 0};
  flex-direction: ${props => props.inlineLabel ? 'row' : 'column'};
  justify-content: flex-start;
  height: ${props => calculateHeight(props)};
  marginBottom: ${props => props.theme.marginBottom};
  paddingTop : ${props => props.theme.paddingTop};
  paddingRight : ${props => props.theme.paddingRight};
  paddingBottom : ${props => props.theme.paddingBottom};
  paddingLeft : ${props => props.theme.paddingLeft};
`

FormGroupWrapper.defaultProps = {
    theme: defaultTheme,
    componentName: 'FormGroupWrapper'
}

const FormGroup = props => {
    const { border, error, inlineLabel, theme, multiline, numberOfLines, keyboardType, returnKeyType } = props
    const children = React.Children.map(props.children, child => {
        let subsetOfProps = {}
        if (child.props.componentName === 'Input') {
            const inputPropTypes = Object.keys(child.type.propTypes)
            subsetOfProps = _.pick(props, inputPropTypes);
        }

        return React.cloneElement(child, Object.assign({}, child.props, {
            inlineLabel, theme, ...subsetOfProps
        }))
    })

    return (
        <FormGroupWrapper border={border} error={error} inlineLabel={inlineLabel}
            multiline={multiline} numberOfLines={numberOfLines} theme={theme}>
            {children}
        </FormGroupWrapper>
    )
}

FormGroup.propTypes = {
    border: PropTypes.bool,
    error: PropTypes.bool,
}

FormGroup.defaultProps = {
    componentName: 'FormGroup',
    border: true,
    error: false,
    inlineLabel: true,
    numberOfLines: 1,
    multiline: false
}

export default FormGroup