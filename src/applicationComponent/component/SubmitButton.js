import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';


export default class SubmitButton extends Component {

    render() {
        let { handleSubmit, onSubmit, label, pristine, submitting } = this.props;
        return (<View style={styles.btnBlock} >
            <TouchableOpacity onPress={handleSubmit(onSubmit)}>
                <View><Text style={styles.btn}>{label}</Text></View>
            </TouchableOpacity>
        </View >);
    }
}

const styles = StyleSheet.create({
    btnBlock: {
        backgroundColor: '#EB5757',
        borderRadius: 8,
        lineHeight: 20,
        marginTop: 20,
        marginRight: 40,
        marginBottom: 50,
        marginLeft: 30,
        paddingLeft: 20,
        paddingBottom: 20
    },
    btn: {
        fontSize: 15,
        fontWeight: 'bold',
        fontFamily: 'Montserrat',
        textAlign: 'center',
        color: '#FFFFFF',
        textTransform: 'uppercase',
        marginTop: 20,
        marginRight: 40
    }
});