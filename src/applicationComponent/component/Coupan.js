import React, { Component } from "react";
import { View, Text, TouchableOpacity, Image, StyleSheet, Button } from 'react-native';
import { render } from "react-dom";


export default class Coupan extends Component {


    render() {
        return (<View style={{ marginBottom: 40 }} >
            <View style={styles.referalCode}>
                <Text style={styles.referalCodeh4}>Have a Referral?<Text style={styles.referalCodeh4Span}>Apply Coupon</Text></Text>
            </View>
            <View style={styles.couponBlock}>
                <View style={styles.inputBlock}>
                    <Text style={styles.couponBlockInputDivFormControl}>xxxxx </Text>

                    <TouchableOpacity onPress={() => { }}>
                        <View><Text style={styles.couponBlocBbutton}>Apply</Text></View>
                    </TouchableOpacity>
                </View>
            </View>
        </View>)
    }
}


const styles = StyleSheet.create({
    referalCode: {
        marginBottom: 10,
        marginLeft: 30
    },
    referalCodeh4: {
        fontSize: 14,
        color: '#565656'
    },
    referalCodeh4Span: {
        color: '#FF783B',
        fontSize: 14,
        fontWeight: '600',

    },
    inputBlock: {
        top: 30,
        paddingLeft: 30,
        paddingRight: 30,
        flexDirection: 'row'
    },
    couponBlockInputDivFormControl: {
        borderWidth: 0.5,
        borderRadius: 2,
        backgroundColor: '#FFFFFF',
        color: '#565656',
        fontFamily: 'Open Sans, sans-serif',
        width: 65,
        marginRight: 10,
        height: 34
    },
    couponBlocBbutton: {
        width: 30,
        paddingTop: 20,
        paddingRight: 12,
        backgroundColor: '#0094FF',
        color: '#FFFFFF',
        borderRadius: 26,
        fontStyle: 'normal',
        fontWeight: '500',
        fontSize: 12,
        lineHeight: 15,
        textAlign: 'center',
        fontFamily: 'Open Sans'
    },
    couponBlock: {
        paddingRight: 10,
        paddingTop: 0
    }
});