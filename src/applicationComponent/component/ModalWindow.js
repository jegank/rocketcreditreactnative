import React, { Component } from 'react';

import { Platform, StyleSheet, View, Text, Modal, Button, TouchableOpacity, Alert, ScrollView } from 'react-native';

export default class ModalWindow extends Component {

    render() {
        let { alertVisibility, showAlert, children } = this.props;
        return (
            <View style={styles.MainContainer}>
                <Modal
                    visible={alertVisibility}
                    transparent={true}
                    animationType={"fade"}
                    onRequestClose={() => { showAlert(!alertVisibility) }} >
                    <View style={{ flex: 1 }} >{children}</View>
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: (Platform.OS == 'ios') ? 20 : 20
    }



});