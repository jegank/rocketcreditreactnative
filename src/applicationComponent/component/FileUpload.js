import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, Image, TouchableOpacity, ImageBackground, Alert } from 'react-native';
import ImagePicker from "react-native-image-picker";
import fileUpload from '../images/fileUpload.png';
import fileLoad from '../images/fileLoad.png';
import ModalWindow from '../component/ModalWindow';


const DetermineImage = (props) => {

    let { handlePress, fileName, reduxOnChange, showAlert } = props;
    const okButton = () => {
        Alert.alert(

            'Alert Title',
            'My Alert Msg',
            [

                { text: 'File Preview', onPress: () => showAlert() },
                { text: 'File Download', onPress: () => showAlert() },

                { text: 'Remove File', onPress: () => reduxOnChange() },

            ],
            { cancelable: false });
    }



    if (fileName) {
        return (<View >
            <ImageBackground source={fileUpload} style={styles.image} >
                <TouchableOpacity onPress={() => okButton()}>
                    {
                        fileName ? <View><Image source={fileLoad} style={styles.fileLoadImage} /><Text>{fileName}</Text></View>

                            : <View><Text>xxxx</Text></View>
                    }
                </TouchableOpacity>
            </ImageBackground>
        </View >);
    } else {
        return (
            <TouchableOpacity onPress={handlePress}>
                <View >
                    <Image source={fileUpload} style={styles.image} />
                    <Text>{fileName ? fileName : 'Attach File'}</Text>
                </View>
            </TouchableOpacity>

        );
    }
}


export default class App extends Component {


    constructor(props) {
        super(props);
        this.handlePress = this.handlePress.bind(this);
        this.reduxOnChange = this.reduxOnChange.bind(this);
        this.content = this.content.bind(this);
        this.okButton = this.okButton.bind(this);
        this.showAlert = this.showAlert.bind(this);
    }

    state = {
        fileName: "",
        alertVisibility: false,
        uri: ""
    }

    showAlert(alertVisibility) {
        this.setState({ alertVisibility });
    }

    componentDidMount() {
        const { input: { value, onChange } } = this.props;
        this.setState({ fileName: value, uri: value.uri });
    }
    reduxOnChange() {
        const { input: { value, onChange } } = this.props;
        onChange("");
        this.setState({ fileName: "" });
    }
    handlePress() {
        const { input: { value, onChange } } = this.props;
        const options = {
            noData: false
        };
        ImagePicker.launchImageLibrary(options, response => {
            onChange(response);
            this.setState({ fileName: response.fileName, uri: response.uri });

        })
    }

    okButton = () => {
        Alert.alert(

            'Alert Title',
            'My Alert Msg',
            [
                { text: 'Ask me later', onPress: () => console.log('Ask me later pressed') },
                { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                { text: 'OK', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false });
    }



    content(props) {
        return (<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <View style={styles.Alert_Main_View}>


                <Image source={{ uri: props.uri }} style={{ width: 100, height: 100 }} />
                <View style={{ width: '100%', height: 1, backgroundColor: '#fff' }} />
                <View style={{ flexDirection: 'row', height: '30%' }}>
                    <TouchableOpacity
                        style={styles.buttonStyle}
                        onPress={props.okButton}
                        activeOpacity={0.7}
                    >
                        <Text style={styles.TextStyle}> OK </Text>
                    </TouchableOpacity>
                    <View style={{ width: 1, height: '100%', backgroundColor: '#fff' }} />
                    <TouchableOpacity
                        style={styles.buttonStyle}
                        onPress={() => { props.showAlert(!props.alertVisibility) }}
                        activeOpacity={0.7}
                    >
                        <Text style={styles.TextStyle}> CANCEL </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>)
    }

    render() {
        const { input: { value, onChange } } = this.props;
        return (
            <View>
                <DetermineImage showAlert={this.showAlert} reduxOnChange={this.reduxOnChange} fileName={this.state.fileName} handlePress={this.handlePress} />
                <ModalWindow alertVisibility={this.state.alertVisibility} showAlert={this.showAlert}>
                    {this.content({
                        alertVisibility: this.state.alertVisibility,
                        showAlert: (value) => this.showAlert(value),
                        okButton: () => this.okButton(),
                        uri: this.state.uri,
                    })}
                </ModalWindow>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    image: {
        width: 80,
        height: 70,
    },
    fileLoadImage: {
        width: 60,
        height: 50,
    },
    Alert_Main_View: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "white",
        height: 200,
        width: '90%',
        borderWidth: 1,
        borderColor: '#fff',
        borderRadius: 7,
    },

    Alert_Title: {
        fontSize: 25,
        color: "black",
        textAlign: 'center',
        padding: 10,
        height: '28%'
    },

    Alert_Message: {
        fontSize: 22,
        color: "black",
        textAlign: 'center',
        padding: 10,
        height: '42%'
    },

    buttonStyle: {
        width: '50%',
        height: '50%',
        justifyContent: 'center',
        alignItems: 'center'
    },

    TextStyle: {
        color: 'black',
        textAlign: 'center',
        fontSize: 22,
        marginTop: -5
    }
});
