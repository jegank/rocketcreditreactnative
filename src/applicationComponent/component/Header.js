import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import backButton from '../images/arrow-left.png';
import { Header } from 'react-native-elements';


const styles = StyleSheet.create({
    container: {
        paddingTop: 25,
        backgroundColor: '#0194FE'
    },
    headerContent: {
        paddingLeft: 25,
        marginTop: 10,
        paddingBottom: 15
    },
    arrow: {
        paddingLeft: 20
    },
    image: {
        width: 40,
        height: 40
    },
    h2: {
        fontSize: 24,
        lineHeight: 29,
        color: "#FFFFFF",
        fontWeight: 'normal'
    }
});




export default class HeaderComponent extends Component {

    handlePress() {
        this.props.backPage();
    }

    render() {
        let { label, helpText } = this.props;
        return (<View style={styles.container}>
            <TouchableOpacity onPress={() => { this.handlePress() }}>
                <View style={styles.arrow} >
                    <Image source={backButton} style={styles.image} />
                </View>
            </TouchableOpacity>
            <View style={styles.headerContent}>
                <Text style={styles.h2}>{label}</Text>
                <Text style={styles.p}>{helpText}</Text>
            </View>
        </View>);
    }
}