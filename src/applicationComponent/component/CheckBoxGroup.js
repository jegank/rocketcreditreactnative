import * as React from "react";
import { CheckBox } from "react-native-elements";

export default class CheckboxGroup extends React.Component {


    componentDidMount() {
        const { input: { value }, options } = this.props;
        if (options) {
            this.setState({ options, value });
        }
    }

    state = {
        options: [],
        value: {}
    }


    onPress = (optionName, checked) => {
        const { input } = this.props;
        if (checked) {
            this.setState({ value: { ...this.state.value, [optionName]: !checked } });
            input.onChange({ ...this.state.value, [optionName]: !checked });
        } else {
            this.setState({ value: { ...this.state.value, [optionName]: !checked } });
            input.onChange({ ...this.state.value, [optionName]: !checked });
        }
    };

    render() {
        const { input: { value }, options } = this.props;
        return (
            <React.Fragment>
                {
                    this.state.options.map(option => {
                        const key = this.state.value.hasOwnProperty(option.name);
                        const checked = key ? this.state.value[option.name] : false;
                        return (
                            <CheckBox
                                key={option.name}
                                title={option.label}
                                checked={checked}
                                onPress={() => this.onPress(option.name, checked)}
                            />
                        );
                    })
                }
            </React.Fragment>
        );
    }
}