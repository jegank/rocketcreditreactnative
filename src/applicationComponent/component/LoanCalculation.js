import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, TouchableOpacity } from 'react-native';


export default class LoanCalculation extends Component {

    constructor(props) {
        super(props);
        this.dueAmountCalculation = this.dueAmountCalculation.bind(this);
    }

    state = {
        dueDate: "------",
        total: "-------"
    }

    dueAmountCalculation(props) {
        let { tenureValue, loanAmount } = props;
        let d = new Date();
        d.setDate(d.getDate() + tenureValue);
        var dateFormated = d.toISOString().substr(0, 10);
        var dueDate = dateFormated;
        let interest1 = (loanAmount * 95) / (100 * 100);
        var interest = (interest1 * tenureValue) / 30;
        var total = loanAmount + interest + 200;
        return { dueDate, total, interest, loanAmount };
    }



    render() {

        let { dueDate, total, interest, loanAmount } = this.dueAmountCalculation(this.props)

        return (<View style={styles.loanSummary}>
            <Text style={styles.h2}>Summary</Text>
            <View style={styles.summaryDetails}>
                <View style={styles.rowSpb}>
                    <Text style={styles.loanAmountSpan}>Amount</Text>
                    <Text style={styles.span}>₹{loanAmount}</Text>
                </View>
                <View style={styles.rowSpb}>
                    <Text style={styles.intrestRatePSpan}>Interest (9.5%)</Text>

                    <Text style={styles.span}>₹{parseInt(interest)}</Text>
                </View>
                <View style={styles.rowSpb}>

                    <Text style={styles.p}>Processing Fee</Text>

                    <Text style={styles.span}>₹200.0</Text>
                </View>
                <View style={styles.rowSpb}>
                    <Text style={styles.dueDatePSpan}>Due Date</Text>

                    <Text style={styles.span} >{dueDate}</Text>
                </View>
            </View>
            <View style={styles.rowSpb}>
                <Text style={styles.totalAmount1}>TOTAL AMOUNT PAYABLE</Text>
                <Text style={styles.totalAmount}>₹{parseInt(total)}</Text>
            </View>
        </View>);
    }
}

const styles = StyleSheet.create({
    totalAmount: {
        height: 40,
        paddingTop: 20,
        paddingRight: 10,
        backgroundColor: '#F6FAFA',
        color: '#3484e0',
        borderRadius: 5,
        borderTopLeftRadius: 0,
        fontWeight: 'bold',
        borderTopRightRadius: 0,
        fontSize: 18
    },
    totalAmount1: {
        height: 40,
        paddingTop: 20,
        paddingRight: 10,
        backgroundColor: '#F6FAFA',
        //   color: '#3484e0',
        borderRadius: 5,
        borderTopLeftRadius: 0,
        borderTopRightRadius: 0,
        fontWeight: 'bold'

    },
    p: {
        fontSize: 18
    },
    rowSpb: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        fontSize: 18
    },
    span: {
        fontSize: 18,
        color: '#565656'
    },
    loanAmountSpan: {
        fontSize: 18,
        color: 'black',
        fontWeight: '800'
    },
    intrestRatePSpan: {
        fontSize: 18,
        color: '#E91B1B'
    },
    dueDatePSpan: {
        fontSize: 18,
        color: '#E91B1B'
    },
    h2: {
        color: 'black',
        fontFamily: 'Montserrat sans-serif',
        fontStyle: 'normal',
        fontWeight: '900',
        fontSize: 18,
        lineHeight: 17,
        textTransform: 'uppercase',
        paddingBottom: 30
    },
    loanSummary: {
        marginBottom: 20
    }
})