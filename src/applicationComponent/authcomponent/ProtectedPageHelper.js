import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image } from 'react-native';
import { _isSignedIn } from './googleSignin';
import Home from '../../pages/home/index';

export const RestrictedComponent = (component) => {
    return _isSignedIn ? { ...component } : Home;
}
