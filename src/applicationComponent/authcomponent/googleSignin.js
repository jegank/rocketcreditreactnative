import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image } from 'react-native';
import {
    GoogleSignin,
    GoogleSigninButton,
    statusCodes,
} from '@react-native-community/google-signin';

import { Actions } from 'react-native-router-flux';
import { Router, Stack, Scene } from "react-native-router-flux";
import { connect } from "react-redux";
import { loginUser } from "../../redux/actions/auth.actions";

export const _getCurrentUser = async () => {
    const currentUser = await GoogleSignin.getCurrentUser();
    return currentUser;
}



export const _isSignedIn = async () => {
    const isSignedIn = await GoogleSignin.isSignedIn();
    return isSignedIn;
}

// export const RestrictedComponent = (props) => {
//     return _isSignedIn() ? <React.Fragment>{props}</React.Fragment> : <View><Text>Restricted</Text></View>;
// }

export const _signOut = async () => {
    if (_isSignedIn) {
        try {
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
            // Remember to remove the user from your app's state as well
        } catch (error) {
            console.error("------->" + error);
        }
    }
    return true;
};

export const _getCurrentUserInfo = async () => {
    let userInfo;
    try {
        userInfo = await GoogleSignin.signInSilently();
    } catch (error) {
        if (error.code === statusCodes.SIGN_IN_REQUIRED) {
            // user has not signed in yet
        } else {
            // some other error
        }
    }
    return userInfo;
};

class GoogleSignIn extends Component {

    state = {
        isSigninInProgress: false,
        loggedIn: false
    }

    componentDidMount() {
        try {
            GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true });
            // google services are available
        } catch (err) {
            console.error('play services are not available');
        }
        GoogleSignin.configure({
            scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
            webClientId: '295029360171-oi4mtr6rm0673pijkgo1a1rjrheoigoh.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
            offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
            hostedDomain: '', // specifies a hosted domain restriction
            loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
            forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
            accountName: '', // [Android] specifies an account name on the device that should be used
            iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
        });
    }



    signIn = async () => {
        // this.setState({ isSigninInProgress: true })
        // if (_isSignedIn) {
        //     Actions.details();
        // } else {
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            this.props.loginUser(userInfo);
            Actions.details();
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                console.log("user cancelled the login flow")
            } else if (error.code === statusCodes.IN_PROGRESS) {

                console.log(" // operation (e.g. sign in) is in progress already")
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
                console.log("  // play services not available or outdated")
            } else {

                console.log("   // some other error happened", error.code)
            }

        }
    };

    isSignedIn = async () => {
        const isSignedIn = await _isSignedIn();
        this.setState({ isLoginScreenPresented: !isSignedIn });
    };

    getCurrentUserInfo = async () => {
        try {
            const userInfo = await _getCurrentUserInfo();
            this.setState({ userInfo });
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_REQUIRED) {
                // user has not signed in yet
            } else {
                // some other error
            }
        }
    };

    getCurrentUser = async () => {
        const currentUser = await _getCurrentUser();
        this.setState({ currentUser });
    };

    signOut = async () => {
        try {
            await _signOut();
            this.setState({ user: null }); // Remember to remove the user from your app's state as well
        } catch (error) {
            console.error(error);
        }
    };


    render() {
        let { } = this.props;
        return (<View>
            <GoogleSigninButton
                style={{ width: 192, height: 48 }}
                size={GoogleSigninButton.Size.Wide}
                color={GoogleSigninButton.Color.Dark}
                onPress={this.signIn}
                disabled={this.state.isSigninInProgress} />
        </View>
        )
    }

}

const mapStateToProps = state => {


    return {

    }
}

const mapDispatchToProps = dispatch => {
    return {
        loginUser: (userInfo) => dispatch(loginUser(userInfo))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GoogleSignIn);



