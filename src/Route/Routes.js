import React, { Component } from "react";
import { Router, Stack, Scene } from "react-native-router-flux";
import GoogleSignin from '../applicationComponent/authcomponent/googleSignin';
import { RestrictedComponent } from '../applicationComponent/authcomponent/ProtectedPageHelper';
import SimpleLogin from "../pages/home/SimpleLogin";
import HassleFreeVerification from "../pages/home/hassleFreeVerification";
import Home from "../pages/home/index";
import details from '../pages/components/details/details';
import TakeLoan from '../pages/components/take-loan/takeloan';
import VerifyDetails from '../pages/components/verify-details/verify-details';
import VerifyDetailsGovProof from '../pages/components/verify-details/verify-details-govt-proof';
import VerifyDetailsReferences from '../pages/components/verify-details/verify-details-references';
import ApplicationStatus from '../pages/components/application-status/application-status';
import Rejected from '../pages/components/rejected/rejected';

export default class Routes extends Component<{}> {
	render() {
		return (
			<Router>
				<Scene hideNavBar={true}>
					<Scene key="root" hideNavBar={true} initial={true}>
						<Scene key="home" initial={true} component={Home} />
						<Scene key="simpleLogin" component={SimpleLogin} />
						<Scene key="hassleFreeVerification" component={HassleFreeVerification} />
						<Scene key="googleSignin" component={GoogleSignin} />
						<Scene key="details" component={details} />
						<Scene key="takeLoan" component={TakeLoan} />
						<Scene key="verifyDetails" component={VerifyDetails} />
						<Scene key="verifyDetailsGovProof" component={VerifyDetailsGovProof} />
						<Scene key="verifyDetailsReferences" component={VerifyDetailsReferences} />
						<Scene key="applicationStatus" component={ApplicationStatus} />
						<Scene key="rejected" component={Rejected} />
					</Scene>
				</Scene>
			</Router>
		);
	}
}
