import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TouchableOpacity
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from "react-redux";
import Routes from './Route/Routes';
//import LoginController from "../LoginController"
class Main extends Component<{}> {
  render() {
    const { authData: { isLoggedIn } } = this.props;
    return (
      <Routes isLoggedIn={isLoggedIn} />
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
const mapStateToProps = state => ({
  authData: state.authReducer.authData
})
export default connect(mapStateToProps, null)(Main)
