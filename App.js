/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
//import LoginController from './LoginController';
import AppRoot from './AppRoot';

const App = () => {
  return (<AppRoot />);
};

// <LoginController />

export default App;
